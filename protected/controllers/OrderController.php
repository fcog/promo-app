<?php

class OrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow authenticated users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$order=new Order;
		$orderxmaterial = new Orderxmaterial;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Order']))
		{
			$trans = Yii::app()->db->beginTransaction();

			try
			{
				$order->user_id = Yii::app()->user->pk;
				$order->destination_id=$_POST['Order']['destination_id'];

				if(!$order->save())
					throw new Exception('Error saving');

				$orderId = $order->id;

				$orderxmaterial->order_id=$orderId;
				$orderxmaterial->material_id=$_POST['Orderxmaterial']['material_id'];
				$orderxmaterial->attributes=$_POST['Orderxmaterial'];

				if($orderxmaterial->save())
					$trans->commit();
				else
					throw new Exception('Error saving');
			}
	    	catch(exception $e)
	    	{
	    		$trans->rollback();
	    		Yii::log("Error saving the problem. Rolling back... Exception: " . $e->getMessage(), CLogger::LEVEL_ERROR, __METHOD__);
                Yii::app()->user->setFlash('error', Yii::t("main", "An Error ocurred saving the order, please try again."));

                $this->redirect(array('create'));         
    		}

    		$this->redirect(array('view','id'=>$order->id));
		}

		$this->render('create',array(
			'order'=>$order,
			'orderxmaterial'=>$orderxmaterial,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$order=$this->loadModel($id);
		$orderxmaterial=new Orderxmaterial;

		$orderxmaterial=Orderxmaterial::model()->findByAttributes(array('order_id'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Order']))
		{
			$trans = Yii::app()->db->beginTransaction();

			try
			{
				$order->destination_id=$_POST['Order']['destination_id'];

				if(!$order->save())
					throw new Exception('Error saving');

				$orderId = $order->id;

				$orderxmaterial->order_id=$orderId;
				$orderxmaterial->material_id=$_POST['Orderxmaterial']['material_id'];
				$orderxmaterial->attributes=$_POST['Orderxmaterial'];

				if($orderxmaterial->save())
					$trans->commit();
				else
					throw new Exception('Error saving');
			}
	    	catch(exception $e)
	    	{
	    		$trans->rollback();
	    		Yii::log("Error updating the problem. Rolling back... Exception: " . $e->getMessage(), CLogger::LEVEL_ERROR, __METHOD__);
                Yii::app()->user->setFlash('error', Yii::t("main", "An Error ocurred updating the order, please try again."));

                $this->redirect(array('create'));         
    		}

    		$this->redirect(array('view','id'=>$order->id));
		}

		$this->render('update',array(
			'order'=>$order,
			'orderxmaterial'=>$orderxmaterial,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isAdmin())
			$dataProvider=new CActiveDataProvider('Order');
		else
			$dataProvider=new CActiveDataProvider('Order', array(
																'criteria'=>array(
																				'condition'=>'user_id='.Yii::app()->user->pk
																				)
																)
												 );

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Order('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Order']))
			$model->attributes=$_GET['Order'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Order the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Order::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Order $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
