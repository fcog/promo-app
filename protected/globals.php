<?php

function isAdmin(){
	return (Yii::app()->user->id && Yii::app()->user->access_level == 6);
}