<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $date_registered
 * @property string $username
 * @property string $password
 *
 * The followings are the available model relations:
 * @property Orders[] $orders
 */
class User extends CActiveRecord
{
	const LEVEL_NORMAL=1, LEVEL_ADMIN=6;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, address, username, password, email, phone, access_level', 'required'),
			array('name', 'length', 'max'=>100),
			array('email', 'length', 'max'=>100),
			array('phone', 'length', 'max'=>100),
			array('access_level', 'length', 'max'=>100),
			array('email', 'email'),
			array('address', 'length', 'max'=>200),
			array('username', 'length', 'max'=>45),
			array('password', 'length', 'max'=>254),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, address, phone, date_registered, username', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orders' => array(self::HAS_MANY, 'Orders', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'User Name',
			'address' => 'Address',
			'email' => 'Email',
			'phone' => 'Phone',
			// 'date_registered' => 'Date Registered',
			'username' => 'Username',
			'password' => 'Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('date_registered',$this->date_registered,true);
		$criteria->compare('username',$this->username,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return boolean validate user
	 */
	public function validatePassword($password, $username){
	        return $this->hashPassword($password, $this->email) === $this->password;
	}
	/**
	 * @return hashed value
	 */

	public function hashPassword($phrase, $salt = null){
	        $key = 'Gf;B&yXL|beJUf-K*PPiU{wf|@9K9j5?d+YW}?VAZOS%e2c -:11ii<}ZM?PO!96';
	        if($salt == '')
	                $salt = substr(hash('sha512', $key), 0, 10);
	        else
	                $salt = substr($salt, 0, 10);
	        return hash('sha512', $salt . $key . $phrase);
	}

	/**
	* define the label for each level
	* @param int $level the level to get the label or null to return a list of labels
	* @return array|string
	*/
	static function getAccessLevelList( $level = null ){
		$levelList=array(
			self::LEVEL_NORMAL => 'Normal User',
			self::LEVEL_ADMIN => 'Administrator'
		);

		if( $level === null)
			return $levelList;

		return $levelList[ $level ];
	}

	static function getUserList(){

		$list = array();

		foreach (User::model()->findAll() as $key => $value) {
			$list[] = array($value['id']=>$value['name']);
		}

		return User::model()->findAll();
	}	
}
