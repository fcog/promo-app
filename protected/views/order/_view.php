<?php
/* @var $this OrderController */
/* @var $data Order */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_order')); ?>:</b>
	<?php echo CHtml::encode($data->date_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('destination_id')); ?>:</b>
	<?php echo CHtml::encode($data->destination->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user->name); ?>
	<br />

	<b>Materials:</b>
	<?php echo CHtml::encode($data->orderxmaterial[0]->material->name); ?>
	<?php //echo var_dump($data->materials) ?>
	<br />	

	<b>Quantity:</b>
	<?php echo CHtml::encode($data->orderxmaterial[0]->quantity); ?>
	<?php //echo var_dump($data->orderxmaterial[0]->material) ?>
	<br />	


</div>