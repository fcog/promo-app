<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Order', 'url'=>array('index')),
	array('label'=>'Create Order', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#order-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Orders</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'order-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'columns'=>array(
		'id',
		'date_order',
		array(
			// 'filter'=>CHtml::activeTextField($model, 'destination_id'),
			'header'=>'Destination',
			'value'=>'$data->destination->name',
		),
		'user.name',
		array(
        	'header'=>'Material',
        	'value'=>'$data->orderxmaterial[0]->material->name',
	    ),
		array(
        	'header'=>'Material quantity',
        	'value'=>'$data->orderxmaterial[0]->quantity',
	    ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
