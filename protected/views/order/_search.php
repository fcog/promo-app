<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_order'); ?>
		<?php echo $form->textField($model,'date_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'destination_id'); ?>
		<?php echo $form->dropDownList($model,'destination_id', CHtml::listData(Destination::model()->getDestinationList(),'id', 'name'), array('empty'=>'Select...')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model,'user_id', CHtml::listData(User::model()->getUserList(),'id', 'name'), array('empty'=>'Select...')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->