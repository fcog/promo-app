<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary(array($order, $orderxmaterial)); ?>

	<div class="row">
		<?php echo $form->labelEx($order,'destination_id'); ?>
		<?php echo $form->dropDownList($order,'destination_id', CHtml::listData(Destination::model()->getDestinationList(),'id', 'name'), array('empty'=>'Select...')); ?>
		<?php echo $form->error($order,'destination_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($orderxmaterial,'material_id'); ?>
		<?php echo $form->dropDownList($orderxmaterial,'material_id', CHtml::listData(Material::model()->getMaterialList(),'id', 'name'), array('empty'=>'Select...')); ?>
		<?php echo $form->error($orderxmaterial,'material_id'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($orderxmaterial,'quantity'); ?>
		<?php echo $form->textField($orderxmaterial,'quantity',array('size'=>10,'maxlength'=>2)); ?>
		<?php echo $form->error($orderxmaterial,'quantity'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($order->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->